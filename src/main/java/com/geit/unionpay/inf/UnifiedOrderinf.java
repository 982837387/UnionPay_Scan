package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface UnifiedOrderinf {
	
	Map<String, Object> CreateUrl(Map<String, Object> reqmap);	//生成url链接


	String WXCreateUrl(Map<String, Object> reqmap);//WX生成url链接

	String CreatefailureUrl(Map<String, Object> reqmap);//错误链接

	String CreateOrderID();

	String UnifiedOrder(Map<String, Object> reqmap) throws UnsupportedEncodingException;


}
