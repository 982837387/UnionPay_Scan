package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface Refundinf {
	 //订单退款接口

	Map<String, Object> Refund(Map<String, Object> map) throws UnsupportedEncodingException;
}
