package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface RefundQueryinf {
	Map<String, Object> RefundQuery(Map<String, Object> map) throws UnsupportedEncodingException;
}
