package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface CloseOrderinf {
	Map<String, Object> CloseOrder(Map<String, Object> map) throws UnsupportedEncodingException;
}
