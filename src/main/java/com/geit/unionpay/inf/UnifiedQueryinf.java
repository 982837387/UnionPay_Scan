package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface UnifiedQueryinf {
	
	Map<String,Object> QueryInfo(Map<String,Object> map) throws UnsupportedEncodingException; //查询订单接口
}
