package com.geit.unionpay.inf;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public interface Union_ScanPayinf {
	//获取支付二维码
	Map<String, Object> Scan_UnifiedOrder(Map<String, Object> reqmap) throws UnsupportedEncodingException;

}
