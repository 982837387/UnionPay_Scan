package com.geit.unionpay.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.geit.unionpay.communication.UnionPayRequest;
import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.inf.CloseOrderinf;
import com.geit.unionpay.utils.GGitUtil;
import com.geit.unionpay.utils.PayUtil;
@Service
public class CloseOrderServiceimpl implements CloseOrderinf {
	private final static Logger logger = LoggerFactory.getLogger(CloseOrderServiceimpl.class);
	@Value("${para.mid}")
	private String mid;
	
	@Value("${para.tid}")
	private String tid;
	
	@Value("${para.instMid}")
	private String instMid;
	
	@Value("${para.msgSrc}")
	private String msgSrc;
	
	@Value("${para.msgSrcId}")
	private String msgSrcId;
	
	@Autowired
	private UnionPayRequest unionpayrequest;

	@Override
	public Map<String, Object> CloseOrder(Map<String, Object> map) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		
		logger.info("------------------close order--------------------------");
		Map<String, Object> reqmap = new HashMap<String, Object>(); //请求map
		Map<String, Object> resp = new HashMap<String, Object>();	//响应resp
		reqmap.put("mid", map.get("mid").toString());				//1商户号
		reqmap.put("tid", map.get("tid").toString());				//2终端号
		reqmap.put("instMid", map.get("instMid").toString());		//3业务类型
		reqmap.put("msgSrc", map.get("msgSrc").toString());			//4消息来源				
		reqmap.put("msgType", "close");								//5消息类型
		
		String aligetTime = PayUtil.aligetTime();
		logger.info("请求时间aligetTime = " + aligetTime);
		reqmap.put("requestTimestamp", aligetTime);					//6报文请求时间	
		reqmap.put("merOrderId", map.get("merOrderId").toString());	//7原交易订单号
		
		//生成待签名字符串并进行MD5加密
		String builderSignStr = "";
		try {
			  builderSignStr = PayUtil.builderSignStr(reqmap,UnionPayConstants.MD5KEY);
			//signString = PayUtil.generateSignature(reqmap, UnionPayConstants.MD5KEY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reqmap.put("sign", builderSignStr.toLowerCase());							//8签名,转小写
		logger.info("reqmap= " + reqmap);
		
		String jsonstring = GGitUtil.MapToJson2(reqmap); //请求map转成json string
		logger.info("发送CloseOrder post请求消息：" + jsonstring);
		
		//接收银联商务返回map
		resp = unionpayrequest.dopost(UnionPayConstants.queryURL, jsonstring);
		return resp;
	}
	
}
