package com.geit.unionpay.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.geit.unionpay.communication.UnionPayRequest;
import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.inf.Union_ScanPayinf;
import com.geit.unionpay.utils.GGitUtil;
import com.geit.unionpay.utils.PayUtil;
@Service
public class Scan_UnionPayimpl implements Union_ScanPayinf{
	private final static Logger logger = LoggerFactory.getLogger(UnifiedOrderServiceimpl.class);
	@Value("${para.mid}")
	private String mid;
	
	@Value("${para.tid}")
	private String tid;
	
	@Value("${para.instMid}")
	private String instMid;
	
	@Value("${para.msgSrc}")
	private String msgSrc;
	
	@Value("${para.msgSrcId}")
	private String msgSrcId;
	
	@Autowired
	private UnionPayRequest unionpayrequest;
	
	/**
	 * 获取支付二维码
	 */
	@Override
	public Map<String, Object> Scan_UnifiedOrder(Map<String, Object> map) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		Map<String, Object> respmap = new HashMap<String, Object>(); //接收银联商务返回map
		Map<String, Object> reqmap = new HashMap<String, Object>(); //接收银联商务返回map
		reqmap.put("mid",map.get("mid"));				//商户号
		reqmap.put("tid", map.get("tid"));				//终端号
		reqmap.put("instMid",map.get("instMid"));
		reqmap.put("msgSrc", map.get("msgSrc"));			//消息来源
		reqmap.put("msgType", "bills.getQRCode");			//消息类型
		
		//报文请求时间
		String aligetTime = PayUtil.aligetTime();
		logger.info("end_time = " + aligetTime);
		reqmap.put("requestTimestamp", aligetTime);	
		//获取二维码id
		String codeid = GGitUtil.createQrCodeId();
		StringBuffer codebuff = new StringBuffer();
		codebuff.append(this.msgSrcId);
		codebuff.append(codeid);
		reqmap.put("qrCodeId", codebuff);
		
		reqmap.put("notifyUrl", "http://www.baidu.com");	//支付结果通知地址
		reqmap.put("returnUrl", "http://www.baidu.com");	//网页跳转地址
		
		//判断哪种二维码
		if(map.get("QRCodeType").equals("onceQRCode")) {
			reqmap.put("totalAmount",map.get("totalAmount"));	
			reqmap.put("billNo",map.get("billNo"));	
			reqmap.put("billDate",map.get("billDate"));	
		}
		if(reqmap.get("QRCodeType").equals("FixedQRCode")) {
			reqmap.put("totalAmount",map.get("totalAmount"));	
		}

		//生成待签名字符串并进行MD5加密
		String builderSignStr = "";
		try {
			  builderSignStr = PayUtil.builderSignStr(reqmap,UnionPayConstants.MD5KEY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reqmap.put("sign", builderSignStr);
		logger.info("reqmap= " + reqmap);
		String jsonstring = GGitUtil.MapToJson2(reqmap); 	//请求map转成json
		logger.info("发送query post请求消息：" + jsonstring);
		
		//接收银联商务返回map
		respmap = unionpayrequest.dopost(UnionPayConstants.queryURL, jsonstring);
		return respmap;
	}

}
