package com.geit.unionpay.impl;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import com.geit.unionpay.communication.UnionPayRequest;
import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.inf.Refundinf;
import com.geit.unionpay.utils.GGitUtil;
import com.geit.unionpay.utils.PayUtil;
@Service
public class RefundServiceimpl implements Refundinf{
	private final static Logger logger = LoggerFactory.getLogger(RefundServiceimpl.class);
	@Value("${para.mid}")
	private String mid;
	
	@Value("${para.tid}")
	private String tid;
	
	@Value("${para.instMid}")
	private String instMid;
	
	@Value("${para.msgSrc}")
	private String msgSrc;
	
	@Value("${para.msgSrcId}")
	private String msgSrcId;
	
	@Autowired
	private UnionPayRequest unionpayrequest;
	@Override
	public Map<String, Object> Refund(Map<String, Object> map) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		logger.info("------------------refund--------------------------");
		Map<String, Object> reqmap = new HashMap<String, Object>(); //请求map
		Map<String, Object> resp = new HashMap<String, Object>();	//响应resp
		reqmap.put("mid", map.get("mid").toString());			//商户号,前端传入
		reqmap.put("tid", map.get("tid").toString());			//终端号
		reqmap.put("instMid",map.get("instMid").toString());	//业务类型
		reqmap.put("msgSrc", map.get("msgSrc").toString());		//消息来源
		reqmap.put("msgId", "UnionPay_F003");					
		reqmap.put("msgType", "refund");						//消息类型
		
		//报文请求时间
		String aligetTime = PayUtil.aligetTime();
		logger.info("请求时间aligetTime = " + aligetTime);
		reqmap.put("requestTimestamp", aligetTime);	
		reqmap.put("merOrderId", map.get("merOrderId").toString());		//原商户交易订单号，前端传入
		reqmap.put("refundAmount", map.get("refundAmount").toString());	//退款金额,前端传入
		
		//下单时未上送分账标识，上传分账标识，需进一步讨论退款金额
		
		reqmap.put("refundDesc", "订单退款"); //退款说明
		//生成退款订单号
		String refundorderid = GGitUtil.createOrderID();
		StringBuffer buff = new StringBuffer(); 
		buff.append(this.msgSrcId);	//来源编号3194
		buff.append(refundorderid);
		reqmap.put("refundOrderId", buff.toString());
		
		//生成待签名字符串并进行MD5加密
		String builderSignStr = "";
		try {
			  builderSignStr = PayUtil.builderSignStr(reqmap,UnionPayConstants.MD5KEY);
			//signString = PayUtil.generateSignature(reqmap, UnionPayConstants.MD5KEY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reqmap.put("sign", builderSignStr);
		logger.info("reqmap= " + reqmap);
		
		String jsonstring = GGitUtil.MapToJson2(reqmap); //请求map转成json string
		logger.info("发送refund post请求消息：" + jsonstring);
		
		//接收银联商务返回退款map
		resp = unionpayrequest.dopost(UnionPayConstants.queryURL, jsonstring);
		return resp;
	}
}
