package com.geit.unionpay.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.geit.unionpay.communication.UnionPayRequest;
import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.inf.UnifiedOrderinf;
import com.geit.unionpay.utils.GGitUtil;
import com.geit.unionpay.utils.PayUtil;



@Service
public class UnifiedOrderServiceimpl implements UnifiedOrderinf{
	
	private final static Logger logger = LoggerFactory.getLogger(UnifiedOrderServiceimpl.class);
	
	@Value("${para.mid}")
	private String mid;
	
	@Value("${para.tid}")
	private String tid;
	
	@Value("${para.instMid}")
	private String instMid;
	
	@Value("${para.msgSrc}")
	private String msgSrc;
	
	@Value("${para.msgSrcId}")
	private String msgSrcId;
	
	@Autowired
	private UnionPayRequest unionpayrequest;
	
	/**
	 * 	银联商务支付下单
	 * return map
	 * @throws UnsupportedEncodingException 
	 */
	@Override
	public String UnifiedOrder(Map<String,Object> map) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		Map<String, Object> reqmap = new HashMap<String, Object>(); //请求银联商务map
			reqmap.put("mid", map.get("mid"));			//商户号
			reqmap.put("tid", map.get("tid"));			//终端号
			reqmap.put("instMid", map.get("instMid"));
			reqmap.put("msgSrc", map.get("msgSrc"));	//消息来源
			reqmap.put("msgId", "UnionPay_F001");		//自定义
			reqmap.put("msgType", map.get("msgType"));	//支付类型,前端传入
			
			//报文请求时间
			String aligetTime = PayUtil.aligetTime();
			logger.info("end_time = " + aligetTime);
			reqmap.put("requestTimestamp", aligetTime);	
			
			//商户订单号
			//reqmap.put("msgSrcId", this.msgSrcId);	//来源编号
//			String orderid = GGitUtil.createOrderID();
//			StringBuffer buff = new StringBuffer(); 
//			buff.append(this.msgSrcId);
//			buff.append(orderid);
			reqmap.put("merOrderId", map.get("merOrderId"));
			
			reqmap.put("originalAmount", map.get("totalAmount"));	//前端传入
			reqmap.put("totalAmount",map.get("totalAmount"));	//订单金额
			reqmap.put("notifyUrl", map.get("notifyUrl"));	//支付结果通知地址
			reqmap.put("returnUrl", map.get("returnUrl"));	//网页跳转地址
			
			//生成待签名字符串并进行MD5加密
			String builderSignStr = "";
			try {
				  builderSignStr = PayUtil.builderSignStr(reqmap,UnionPayConstants.MD5KEY);
				//signString = PayUtil.generateSignature(reqmap, UnionPayConstants.MD5KEY);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			reqmap.put("sign", builderSignStr);
			logger.info("reqmap= " + reqmap);
			
			//拼接请求URL
			StringBuffer url = new StringBuffer();
			try {
				url.append("https://qr.chinaums.com/netpay-portal/webpay/pay.do?");
				url.append("requestTimestamp=" + URLEncoder.encode((String) reqmap.get("requestTimestamp"), "UTF-8") +
						"&mid=" + URLEncoder.encode((String) reqmap.get("mid"), "UTF-8") + 
						"&tid="+ URLEncoder.encode((String) reqmap.get("tid"), "UTF-8") + 
						"&instMid=" + URLEncoder.encode((String) reqmap.get("instMid"), "UTF-8") + 
						"&msgSrc=" + URLEncoder.encode((String) reqmap.get("msgSrc"), "UTF-8") + 
						"&merOrderId=" + URLEncoder.encode((String) reqmap.get("merOrderId"), "UTF-8") + 
						"&totalAmount=" + URLEncoder.encode((String) reqmap.get("totalAmount"), "UTF-8") + 
						"&msgId=" + URLEncoder.encode((String) reqmap.get("msgId"), "UTF-8") + 
						"&msgType=" + URLEncoder.encode((String) reqmap.get("msgType"), "UTF-8") + 
						"&originalAmount=" + URLEncoder.encode((String) reqmap.get("originalAmount"), "UTF-8") + 
						"&notifyUrl=" + URLEncoder.encode((String) reqmap.get("notifyUrl"), "UTF-8") + 
						"&returnUrl=" + URLEncoder.encode((String) reqmap.get("returnUrl"), "UTF-8") + 
						"&sign=" + URLEncoder.encode((String) reqmap.get("sign"), "UTF-8"));
				logger.info("银联商务下单url = " + url);
			} catch (Exception e) {
				// TODO: handle exception
				return "缺少必要参数，请核实后再进行下单";
			}
		return url.toString();
	}
	/**
	 * 生成支付链接+商户号
	 */
	@Override
	public Map<String, Object> CreateUrl(Map<String, Object> reqmap) {
		// TODO Auto-generated method stub
		
		//生成商户订单号
		//reqmap.put("msgSrcId", this.msgSrcId);	//来源编号
//		String orderid = GGitUtil.createOrderID();
//		StringBuffer buff = new StringBuffer(); 
//		buff.append(this.msgSrcId);
//		buff.append(orderid);
//		String merOrderId = buff.toString();
		
		//拼接请求URL
		String mid = (String) reqmap.get("mid");
		String tid = (String) reqmap.get("tid");
		String instMid = (String) reqmap.get("instMid");
		String msgSrc = (String) reqmap.get("msgSrc");
		String totalAmount = (String) reqmap.get("totalAmount");
		String msgType = (String) reqmap.get("msgType");
		String notifyUrl = (String) reqmap.get("notifyUrl");
		String returnUrl = (String) reqmap.get("returnUrl");
		String merOrderId = (String) reqmap.get("merOrderId");
		StringBuffer url = new StringBuffer();
		url.append("http://www.ggzzrj.cn:8081/unionpay/lay/unifiedpay?");
		url.append("mid=" + mid + "&tid=" + tid + "&instMid=" + instMid + "&msgSrc=" + msgSrc 
				+ "&totalAmount=" + totalAmount + "&msgType=" + msgType + "&notifyUrl=" + notifyUrl +"&returnUrl=" + returnUrl + "&merOrderId=" + merOrderId);
		
		Map<String,Object> respmap = new HashMap<String,Object>();
		respmap.put("merOrderId", merOrderId);
		respmap.put("url", url.toString());
		return respmap;
	}
	
	/**
	 * 公众号生成支付链接+商户号
	 */
	@Override
	public String WXCreateUrl(Map<String, Object> reqmap) {
		// TODO Auto-generated method stub
		
		//生成商户订单号
		//reqmap.put("msgSrcId", this.msgSrcId);	//来源编号
		String orderid = GGitUtil.createOrderID();
		StringBuffer buff = new StringBuffer(); 
		buff.append(this.msgSrcId);
		buff.append(orderid);
		String merOrderId = buff.toString();
		
		//拼接请求URL
		String mid = (String) reqmap.get("mid");
		String tid = (String) reqmap.get("tid");
		String instMid = (String) reqmap.get("instMid");
		String msgSrc = (String) reqmap.get("msgSrc");
		String totalAmount = (String) reqmap.get("totalAmount");
		String msgType = (String) reqmap.get("msgType");
		String notifyUrl = (String) reqmap.get("notifyUrl");
		String returnUrl = (String) reqmap.get("returnUrl");
		StringBuffer url = new StringBuffer();
		url.append("http://172.20.10.2:8080/UnionPay/lay/unionpayhtml?");
		url.append("mid=" + mid + "&tid=" + tid + "&instMid=" + instMid + "&msgSrc=" + msgSrc 
				+ "&totalAmount=" + totalAmount + "&msgType=" + msgType + "&notifyUrl=" + notifyUrl +"&returnUrl=" + returnUrl + "&merOrderId=" + merOrderId);
	
		return url.toString();
	}
	
	/**
	 * 公众号失败页面
	 */
	@Override
	public String CreatefailureUrl(Map<String, Object> reqmap) {
		// TODO Auto-generated method stub
		//拼接请求URL
		StringBuffer url = new StringBuffer();
		url.append("http://172.20.10.2:8080/UnionPay/lay/failure?");
		url.append("returnCode=" + reqmap.get("returnCode"));
		return url.toString();
	}
	
	/**
	 * 公众号生成商户号
	 */
	@Override
	public String CreateOrderID() {
		// TODO Auto-generated method stub
		
		//生成商户订单号
		//reqmap.put("msgSrcId", this.msgSrcId);	//来源编号
		String orderid = GGitUtil.createOrderID();
		StringBuffer buff = new StringBuffer(); 
		buff.append(this.msgSrcId);
		buff.append(orderid);
		String merOrderId = buff.toString();
		logger.info("CreateOrderID merOrderId=" + merOrderId);
		return merOrderId;
		
	}
}
