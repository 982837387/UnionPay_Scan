package com.geit.unionpay.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.impl.UnifiedOrderServiceimpl;
import com.geit.unionpay.utils.JsonUtils;
import com.geit.unionpay.utils.PayUtil;

@RestController
@CrossOrigin
@RequestMapping("/lay")	
public class UnifiedPayScanController {
	private final static Logger logger = LoggerFactory.getLogger(UnifiedPayScanController.class);
	
	@Autowired
	private UnifiedOrderServiceimpl unionpayserviceimpl;
	/**
	 * 扫码付下单接口
	 * @param request
	 * @param response
	 * @param jsonreq
	 * @return
	 */
	@RequestMapping(value = "/UnifiedOrder", method = RequestMethod.POST)
	public Map<String, Object> unionpayorder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody String jsonreq) {
		
		Map<String,Object> reqmap = new HashMap<String,Object>();	//客户端原始请求map
		Map<String,Object> datamap = new HashMap<String,Object>();	//客户端原始请求Data数据
		Map<String,Object> resultmap = new HashMap<String,Object>();	//返回结果
		//接收客户端请求数据并转换成map
		reqmap = JsonUtils.JsonToMapObj(jsonreq);
		logger.info("unifiedquery reqmap = " + reqmap);
		
		datamap = (Map<String, Object>) reqmap.get("tradeParam");	//请求体数据
		logger.info("unifiedquery datamap = " + datamap);
		//---------------------step1  验证签名-----------------------------
		String sign = (String) reqmap.get("sign");		//获取sign
		String key = UnionPayConstants.GGMD5KEY;		//国光MD5密钥
		try {
			if (!PayUtil.verifySign(datamap,key,sign)) {
				resultmap.put("returnInfo", "签名错误");
				resultmap.put("returnCode", "Bad_Sign");
				
				return resultmap;
			}
		//----------------------step2  验证消息类型  tradeType:UnifiedOrder--------
			if (!"UnifiedOrder".equals(reqmap.get("tradeType"))) {
				resultmap.put("returnCode", "TradeType_Error");
				resultmap.put("returnInfo", "消息类型不符");
				return resultmap;
			}
		//-----------------------step3  验证传参完整性-------------------------- 
			//验证公共参数完整性
			if(!PayUtil.verifyParameter(datamap)) {
				resultmap.put("returnCode", "Common_Value_Error");
				resultmap.put("returnInfo", "缺少必要公共参数");
				return resultmap;
			}
			//验证接口参数完整性
			if(datamap.get("totalAmount").equals("") || datamap.get("msgType").equals("") 
					|| datamap.get("notifyUrl").equals("") || datamap.get("returnUrl").equals("") ||datamap.get("merOrderId").equals("") ) {
				resultmap.put("returnCode", "Value_Error");
				resultmap.put("returnInfo", "缺少必要接口参数");
				return resultmap;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultmap.put("returnCode", "System_Error");
			resultmap.put("returnInfo", "系统异常");
			return resultmap;
		}
		//-----------------------step4 传值并调用支付链接生成接口--------------------------------
		Map<String,Object> resultdatamap = new HashMap<String,Object>();
		resultdatamap.put("mid", datamap.get("mid"));					    //商户号
		resultdatamap.put("tid", datamap.get("tid"));						//终端号
		resultdatamap.put("instMid", datamap.get("instMid"));				//机构商户号
		resultdatamap.put("msgSrc", datamap.get("msgSrc"));					//消息来源
		
//		//分转元
//		String Amount = PayUtil.changeF2Y(datamap.get("totalAmount").toString());
//		resultdatamap.put("totalAmount", Amount);							//支付总金额
		resultdatamap.put("totalAmount", datamap.get("totalAmount")); 		//总金额
		resultdatamap.put("msgType", datamap.get("msgType"));				//支付类型
		resultdatamap.put("notifyUrl", datamap.get("notifyUrl"));			//支付结果通知
		resultdatamap.put("returnUrl", datamap.get("returnUrl"));			//网页跳转地址
		resultdatamap.put("merOrderId", datamap.get("merOrderId"));			//C端上送商户订单号
		Map<String, Object> urlmap = unionpayserviceimpl.CreateUrl(resultdatamap);			//生成支付链接，返回给前端生成二维码
		//-----------------------step5 返回报文，支付链接--------------------------------
		resultmap.put("returnCode", "SUCCESS");	
		//resultmap.put("returnInfo","生成支付链接成功");
		Map<String,Object> urldatamap = new HashMap<String,Object>();
		urldatamap.put("url", urlmap.get("url"));
		//urldatamap.put("merOrderId", urlmap.get("merOrderId"));
		urldatamap.put("merOrderId", datamap.get("merOrderId"));
		
		resultmap.put("data", urldatamap);

		return resultmap;	
	}
	
}
