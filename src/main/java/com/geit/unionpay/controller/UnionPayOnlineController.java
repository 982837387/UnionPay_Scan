package com.geit.unionpay.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.impl.UnifiedOrderServiceimpl;
import com.geit.unionpay.utils.PayUtil;

@Controller
@CrossOrigin
@RequestMapping("/lay")	
public class UnionPayOnlineController {
	private final static Logger logger = LoggerFactory.getLogger(UnionPayOnlineController.class);
	
	@Autowired
	private UnifiedOrderServiceimpl unionpayserviceimpl;
	
	/**
	 * 公众号支付下单接口
	 * 调用该接口，跳转到html支付页面
	 * @param request
	 * @param response
	 * @param jsonreq
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	@RequestMapping(value = "/UnifiedPay", method = RequestMethod.GET)
	public String unionpay(HttpServletRequest request, HttpServletResponse response,Map<String,Object> resultdatamap)  {
		Map<String,Object> datamap = new HashMap<String,Object>();	//客户端请求数据
		String key = UnionPayConstants.GGMD5KEY;					//国光MD5密钥
		
		//---------------------取到URL请求数据----------------------------------------------------
		logger.info("getQueryString:" + request.getQueryString());   //请求url的QueryString
		
			String mid = request.getParameter("mid");
			String tid = request.getParameter("tid");
			String instMid = request.getParameter("instMid");
			String msgSrc = request.getParameter("msgSrc");
			String totalAmount = request.getParameter("totalAmount");
			String msgType = request.getParameter("msgType");
			String notifyUrl = request.getParameter("notifyUrl");
			String returnUrl = request.getParameter("returnUrl");
			String merOrderId = request.getParameter("merOrderId");		//C端上送订单号
			String sign = request.getParameter("sign");
			
		//--------------------step1 对请求字段值进行URLDecoder转码----------------------------
		
			try {
				datamap.put("mid", URLDecoder.decode(mid, "UTF-8"));
				datamap.put("tid", URLDecoder.decode(tid, "UTF-8"));
				datamap.put("instMid", URLDecoder.decode(instMid, "UTF-8"));
				datamap.put("msgSrc", URLDecoder.decode(msgSrc, "UTF-8"));
				datamap.put("totalAmount", URLDecoder.decode(totalAmount, "UTF-8"));
				datamap.put("msgType", URLDecoder.decode(msgType, "UTF-8"));
				datamap.put("notifyUrl", URLDecoder.decode(notifyUrl, "UTF-8"));
				datamap.put("returnUrl", URLDecoder.decode(returnUrl, "UTF-8"));
				datamap.put("merOrderId", URLDecoder.decode(merOrderId, "UTF-8"));
			} catch (Exception e) {
				// TODO: handle exception
				resultdatamap.put("returnInfo", "缺少必要参数");
				resultdatamap.put("returnCode", "Value_Error");
				return "failure";
			}
			logger.info("URLDecoder转码后datamap = " + datamap);
		
		//-------------------------step2验证签名------------------------------------------------------
		try {
			if (!PayUtil.verifySign(datamap,key,sign)) {
				resultdatamap.put("returnInfo", "签名错误");
				resultdatamap.put("returnCode", "Bad_Sign");
				//String failureurl = unionpayserviceimpl.CreatefailureUrl(resultmap);
				//logger.info("url = " + failureurl);
				return "failure";	
			}
		//-----------------------step3  验证传参完整性-------------------------- 
			//验证公共参数完整性
			if(!PayUtil.verifyParameter(datamap)) {
				resultdatamap.put("returnCode", "Common_Value_Error");
				resultdatamap.put("returnInfo", "缺少必要公共参数");
				return "failure";
			}
			//验证接口参数完整性
			if(datamap.get("totalAmount").equals("") || datamap.get("msgType").equals("") 
					|| datamap.get("notifyUrl").equals("") || datamap.get("returnUrl").equals("") ||datamap.get("merOrderId").equals("")) {
				resultdatamap.put("returnCode", "Value_Error");
				resultdatamap.put("returnInfo", "缺少必要接口参数,必传接口参数不允许为空");
				return "failure";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultdatamap.put("returnCode", "System_Error");
			resultdatamap.put("returnInfo", "系统异常");
			return "failure";
		}
		//-----------------------step4 传值并生成链接跳转到支付页面--------------------------------
		//Map<String,Object> resultdatamap = new HashMap<String,Object>();
		resultdatamap.put("mid", mid);					    //商户号
		resultdatamap.put("tid", tid);						//终端号
		resultdatamap.put("instMid", instMid);				//机构商户号
		resultdatamap.put("msgSrc", msgSrc);				//消息来源
		resultdatamap.put("totalAmount", totalAmount); 		//总金额
		resultdatamap.put("msgType", msgType);				//支付类型
		resultdatamap.put("notifyUrl", notifyUrl);			//支付结果通知
		resultdatamap.put("returnUrl", returnUrl);			//网页跳转地址
		//resultdatamap.put("merOrderId", unionpayserviceimpl.CreateOrderID());	//生成商户订单号
		resultdatamap.put("merOrderId", merOrderId);		//C端上送商户订单号
		logger.info("resultdatamap = " + resultdatamap);
		//根据payType判断回调到哪个支付页面
		if(resultdatamap.get("msgType").equals("WXPay.jsPay")) {	//微信支付
			return "wxunionpay";
		}else {
			return "aliunionpay";									//支付宝支付
		}
	}
	
	/**
	 * a支付失败页面
	 * @param request
	 * @param response
	 * @param returnCode
	 * @param map
	 * @return
	 */
	@RequestMapping(value = "/failure", method = RequestMethod.GET)
	public String failure(HttpServletRequest request, HttpServletResponse response,String returnCode,Map<String, Object> map) {
		
		map.put("returnCode", returnCode); 
		
		return "failure";	
	}
	
	/**
	 * a根据支付页面请求，调起支付下单
	 * @param request
	 * @param response
	 * @param totalAmount
	 * @param msgType
	 * @return
	 */
	@RequestMapping(value = "/unifiedpay", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response, Map<String, Object> map) {
		Map<String,Object> reqmap = new HashMap<String,Object>();
		//---------------------取到URL请求数据-----------------------------
		logger.info("unifiedpay getQueryString:" + request.getQueryString());    //请求url的QueryString
			String mid = request.getParameter("mid");
			String tid = request.getParameter("tid");
			String instMid = request.getParameter("instMid");
			String msgSrc = request.getParameter("msgSrc");
			String totalAmount = request.getParameter("totalAmount");
			String YtotalAmount = PayUtil.changeY2F(totalAmount);	//元转分
			logger.info("YtotalAmount = " + YtotalAmount);
			String msgType = request.getParameter("msgType");
			String notifyUrl = request.getParameter("notifyUrl");
			String returnUrl = request.getParameter("returnUrl");
			String merOrderId = request.getParameter("merOrderId");
			
			reqmap.put("mid", mid);
			reqmap.put("tid", tid);
			reqmap.put("instMid", instMid);
			reqmap.put("msgSrc", msgSrc);
			reqmap.put("totalAmount", YtotalAmount);
			reqmap.put("msgType", msgType);
			reqmap.put("notifyUrl", notifyUrl);
			reqmap.put("returnUrl", returnUrl);
			reqmap.put("merOrderId", merOrderId);
			logger.info("reqmap = " + reqmap);
		String url = "";
		try {
			url = unionpayserviceimpl.UnifiedOrder(reqmap);
			//logger.info("请求URL = " + url);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		if(url.equals("缺少必要参数，请核实后再进行下单")) {
//			map.put("returnCode", "缺少必要参数，请核实后再进行下单");
//			return "failure";
//		}
		return "redirect:" + url; //重定向到下单页面	
	}
	
}
