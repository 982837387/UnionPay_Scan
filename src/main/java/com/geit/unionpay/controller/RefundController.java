package com.geit.unionpay.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.impl.RefundServiceimpl;
import com.geit.unionpay.utils.JsonUtils;
import com.geit.unionpay.utils.PayUtil;
@RestController
@CrossOrigin	//解决跨域请求
@RequestMapping("/lay")
public class RefundController {
	private final static Logger logger = LoggerFactory.getLogger(RefundController.class);
	
	@Autowired
	private RefundServiceimpl refundserviceimpl;
	/**
	 * 退款接口
	 * @param request
	 * @param response
	 * @param merOrderId
	 * @param refundAmount
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	@RequestMapping(value = "/OrderRefund", method = RequestMethod.POST)
	public Map<String, Object> query(HttpServletRequest request, HttpServletResponse response, @RequestBody String jsonreq) throws UnsupportedEncodingException {		
		Map<String,Object> map = new HashMap<String,Object>(); 			//接收退款请求map
		Map<String,Object> reqmap = new HashMap<String,Object>();		//客户端原始请求map
		Map<String,Object> datamap = new HashMap<String,Object>();		//客户端原始请求Data数据
		Map<String,Object> resultmap = new HashMap<String,Object>();	//返回结果
		//接收客户端请求数据并转换成map
		reqmap = JsonUtils.JsonToMapObj(jsonreq);
		logger.info("unifiedquery reqmap = " + reqmap);
		
		datamap = (Map<String, Object>) reqmap.get("tradeParam");	//tradeParam请求体数据
		logger.info("unifiedquery datamap = " + datamap);
		
		//-------------------------step1  验证签名-----------------------------
		String sign = (String) reqmap.get("sign");		//获取sign
		String key = UnionPayConstants.GGMD5KEY;		//国光MD5密钥
		try {
			if (!PayUtil.verifySign(datamap,key,sign)) {
				resultmap.put("returnCode", "Bad_Sign");
				resultmap.put("returnInfo", "签名错误");
				return resultmap;
			}
		//-----------------------step2  验证消息类型  tradeType:refund-------------------
			if (!"refund".equals(reqmap.get("tradeType"))) {
				resultmap.put("returnCode", "TradeType_Error");
				resultmap.put("returnInfo", "消息类型不符");
				return resultmap;
			}
		//------------------------step3 验证传参完整性----------------------------------
			//验证公共参数完整性
			if(!PayUtil.verifyParameter(datamap)) {
				resultmap.put("returnCode", "Common_Value_Error");
				resultmap.put("returnInfo", "缺少必要公共参数");
				return resultmap;
			}
			//验证接口参数完整性
			if(datamap.get("merOrderId").equals("") || datamap.get("refundAmount").equals("")) {
				resultmap.put("returnCode", "Value_Error");
				resultmap.put("returnInfo", "缺少必要接口参数");
				return resultmap;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultmap.put("returnCode", "System_Error");
			resultmap.put("returnInfo", "系统异常");
			return resultmap;
		}
		//------------------------step4 传值并调用退款接口--------------------------------
		map.put("mid", datamap.get("mid"));					    //商户号
		map.put("tid", datamap.get("tid"));						//终端号
		map.put("instMid", datamap.get("instMid"));				//机构商户号
		map.put("msgSrc", datamap.get("msgSrc"));				//消息来源
		map.put("merOrderId", datamap.get("merOrderId"));		//原支付订单号
		map.put("refundAmount", datamap.get("refundAmount"));   //退款金额
		Map<String, Object> refundmap = refundserviceimpl.Refund(map);
		logger.info("refundmap = " + refundmap);
		
		if(!"200".equals(refundmap.get("statuscode"))) {
			resultmap.put("returnCode", "Union_Connect_Error");
			resultmap.put("returnInfo", "银联网付连接失败");
			return resultmap;
		}
		//------------------------step5接口返回数据----------------------------------
		if(refundmap.get("errCode").equals("SUCCESS")) {
			
		Map<String,Object> resultdatamap = new HashMap<String,Object>();	//resultmap中data数据
		resultdatamap.put("errCode", refundmap.get("errCode"));				//平台错误码
		resultdatamap.put("status", refundmap.get("status"));				//交易状态
		//分转元
		String Amount = PayUtil.changeF2Y(refundmap.get("totalAmount").toString());
		resultdatamap.put("totalAmount", Amount);							//支付总金额
		resultdatamap.put("targetSys", refundmap.get("targetSys"));			//目标平台代码（第三方代码）
		resultdatamap.put("targetStatus", refundmap.get("targetStatus"));	//目标平台状态
		resultdatamap.put("refundStatus", refundmap.get("refundStatus"));	//支付时间
		resultdatamap.put("refundOrderId", refundmap.get("refundOrderId"));	//平台退款订单号
		resultdatamap.put("refundTargetOrderId", refundmap.get("refundTargetOrderId"));	//第三方退款订单号
		resultdatamap.put("messageType", refundmap.get("msgType"));			//消息类型
		
		resultmap.put("data", resultdatamap);
		resultmap.put("returnCode", refundmap.get("errCode"));	
		//resultmap.put("returnInfo", refundmap.get("errMsg"));
		return resultmap;
		
		}else {
			//错误返回信息
			resultmap.put("returnCode", refundmap.get("errCode"));
			resultmap.put("returnInfo", refundmap.get("errMsg"));
			return resultmap;
		}
	}
}
