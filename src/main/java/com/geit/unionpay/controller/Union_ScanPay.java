package com.geit.unionpay.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.geit.unionpay.config.UnionPayConstants;
import com.geit.unionpay.impl.Scan_UnionPayimpl;
import com.geit.unionpay.impl.UnifiedOrderServiceimpl;
import com.geit.unionpay.utils.JsonUtils;
import com.geit.unionpay.utils.PayUtil;

@RestController
@CrossOrigin
@RequestMapping("/lay")
public class Union_ScanPay {
private final static Logger logger = LoggerFactory.getLogger(UnifiedPayScanController.class);
	
	@Autowired
	private Scan_UnionPayimpl scan_unionPay;
	
	@RequestMapping(value = "/ShowQRCode", method = RequestMethod.POST)
	public Map<String, Object> ShowQRCode(HttpServletRequest request, HttpServletResponse response,@RequestBody String jsonreq) throws UnsupportedEncodingException {
		Map<String,Object> reqmap = new HashMap<String,Object>();	//客户端原始请求map
		Map<String,Object> datamap = new HashMap<String,Object>();	//客户端原始请求Data数据
		Map<String,Object> reqdatamap = new HashMap<String,Object>();	//返回结果
		Map<String,Object> resultmap = new HashMap<String,Object>();	//返回结果
		Map<String,Object> resultdatamap = new HashMap<String,Object>();	//返回结果
		//接收客户端请求数据并转换成map
		reqmap = JsonUtils.JsonToMapObj(jsonreq);
		logger.info("unifiedquery reqmap = " + reqmap);
		//请求体数据
		datamap = (Map<String, Object>) reqmap.get("tradeParam");
		logger.info("unifiedquery datamap = " + datamap);
		String totalAmount = (String) datamap.get("totalAmount");
		String billNo = (String) datamap.get("billNo");
		String billDate = (String) datamap.get("billDate");
		String QRCodeType = (String) datamap.get("QRCodeType");
		logger.info("billNo = " + billNo);
		
		//---------------------step1  验证签名-----------------------------
		String sign = (String) reqmap.get("sign");		//获取sign
		String key = UnionPayConstants.GGMD5KEY;		//国光MD5密钥
		try {
			if (!PayUtil.verifySign(datamap,key,sign)) {
				resultmap.put("returnInfo", "签名错误");
				resultmap.put("returnCode", "Bad_Sign");
				
				return resultmap;
			}
		//----------------------step2  验证消息类型  tradeType:UnifiedOrder--------
			if (!"scan_unionpay".equals(reqmap.get("tradeType"))) {
				resultmap.put("returnCode", "TradeType_Error");
				resultmap.put("returnInfo", "消息类型不符");
				return resultmap;
			}
		//-----------------------step3  验证传参完整性-------------------------- 
			//验证公共参数完整性
			if(!PayUtil.verifyParameter(datamap)) {
				resultmap.put("returnCode", "Common_Value_Error");
				resultmap.put("returnInfo", "缺少必要公共参数");
				return resultmap;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			resultmap.put("returnCode", "System_Error");
			resultmap.put("returnInfo", "系统异常");
			return resultmap;
		}
		//---------------step4 请求银联商务所需参数赋值---------------------
		reqdatamap.put("msgSrc",datamap.get("msgSrc"));
		reqdatamap.put("mid",datamap.get("mid"));
		reqdatamap.put("tid",datamap.get("tid"));
		reqdatamap.put("instMid", datamap.get("instMid"));
		reqdatamap.put("notifyUrl", datamap.get("notifyUrl"));
		reqdatamap.put("returnUrl", datamap.get("returnUrl"));
		reqdatamap.put("QRCodeType", datamap.get("QRCodeType"));	//二维码类型

		//根据QRCodeType判断选择生成何种支付二维码
		if(QRCodeType.equals("onceQRCode")) {		//一次性支付二维码
			//验证参数完整性
			if(!totalAmount.equals("") && !(totalAmount == null)) {
				reqdatamap.put("totalAmount", totalAmount);
			}else {
				resultmap.put("returnCode", "Value_err");
				resultmap.put("returnInfo", "totalAmount不能为空值");
				return resultmap;
			}
			if(!billNo.equals("") && !(billNo == null)) {
				reqdatamap.put("billNo", billNo);
			}else {
				resultmap.put("returnCode", "Value_err");
				resultmap.put("returnInfo", "billNo不能为空值");
				return resultmap;
			}
			if(!billDate.equals("") && !(billDate == null)) {
				reqdatamap.put("billDate", billDate);
			}else {
				resultmap.put("returnCode", "Value_err");
				resultmap.put("returnInfo", "billDate不能为空值");
				return resultmap;
			}
			resultmap = scan_unionPay.Scan_UnifiedOrder(reqmap);
			resultdatamap.put("returnCode", resultmap.get("errCode"));
			resultdatamap.put("returnInfo", resultmap.get("errMsg"));
			resultdatamap.put("data",resultmap);
			return resultdatamap;
		}else if(QRCodeType.equals("FixedQRCode")){		//半固定二维码（带金额固定二维码）
			//验证参数完整性
			if(!totalAmount.equals("") && !(totalAmount == null)) {
				reqdatamap.put("totalAmount", totalAmount);
				resultmap = scan_unionPay.Scan_UnifiedOrder(reqmap);
				return resultmap;
			}else {
				resultmap.put("returnCode", "Value_err");
				resultmap.put("returnInfo", "totalAmount不能为空值");
				return resultmap;
			}
		}else {											//固定二维码
			resultmap = scan_unionPay.Scan_UnifiedOrder(reqmap);
			return resultmap;
		}
	}
}
