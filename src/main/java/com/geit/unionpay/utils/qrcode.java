package com.geit.unionpay.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.geit.unionpay.config.UnionPayConstants;


public class qrcode {
	
	public static void main(String[] args) {
		
		
//		//分转元
//		String Amount = PayUtil.changeF2Y("100");
//		System.out.println("Amount = " + Amount);
		
		Map<String,Object> datamap = new HashMap<String,Object>();	//客户端请求数据
		String key = UnionPayConstants.GGMD5KEY;					//国光MD5密钥
		//--------------------------step1 报文签名sign----------------------------
			datamap.put("mid","898340149000005");
			datamap.put("tid","88880001");
			datamap.put("instMid","YUEDANDEFAULT");
			datamap.put("msgSrc","WWW.TEST.COM");
			datamap.put("totalAmount","0.01");				//单位：元
			//datamap.put("msgType","trade.jsPay");
			//datamap.put("msgType","acp.jsPay");
			datamap.put("msgType","WXPay.jsPay");
			datamap.put("notifyUrl","http://www.ggzzrj.cn");
			datamap.put("returnUrl","http://www.ggzzrj.cn");
			
			//生成商户订单号
			//reqmap.put("msgSrcId", this.msgSrcId);	//来源编号
			String orderid = GGitUtil.createOrderID();
			StringBuffer buff = new StringBuffer(); 
			buff.append("3194");
			buff.append(orderid);
			String merOrderId = buff.toString();
			
			datamap.put("merOrderId", merOrderId);
			System.out.println("URLDecoder转码后datamap = " + datamap);
		//签名
		String sign = "";
		try {
			sign = PayUtil.YZbuilderSignStr(datamap, key);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println("sign=" + sign);
		
		//------------------------step2拼接请求URL-------------------------------
		StringBuffer url = new StringBuffer();
		//url.append("http://172.20.10.2:8080/UnionPay/lay/UnifiedPay?");
		url.append("http://www.ggzzrj.cn:8081/unionpay/lay/UnifiedPay?");
		try {
			url.append("mid=" + URLEncoder.encode("898340149000005", "UTF-8") + 
					"&tid=" + URLEncoder.encode("88880001", "UTF-8") + 
					"&instMid=" + URLEncoder.encode("YUEDANDEFAULT", "UTF-8") + 
					"&msgSrc="+ URLEncoder.encode("WWW.TEST.COM", "UTF-8") + 
//					"&payType="+ URLEncoder.encode("trade.jsPay", "UTF-8") +
//					"&payType="+ URLEncoder.encode("acp.jsPay", "UTF-8") +
					"&msgType="+ URLEncoder.encode("WXPay.jsPay", "UTF-8") +
					"&totalAmount="+ URLEncoder.encode("0.01", "UTF-8") +
					"&notifyUrl="+ URLEncoder.encode("http://www.ggzzrj.cn", "UTF-8") +
					"&returnUrl="+ URLEncoder.encode("http://www.ggzzrj.cn", "UTF-8") + 
					"&merOrderId="+ URLEncoder.encode(merOrderId, "UTF-8") + 
					"&sign="+ URLEncoder.encode(sign, "UTF-8"));
			System.out.println("url=" + url);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
