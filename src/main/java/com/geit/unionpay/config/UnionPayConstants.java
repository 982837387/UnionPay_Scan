package com.geit.unionpay.config;

public class UnionPayConstants {
	// 银联商务网关
	public static final String URL = "https://qr-test2.chinaums.com/netpay-portal/webpay/pay.do";	//下单测试环境
	//支付查询、退款、退款查询、订单撤销接口测试环境
	public static final String queryURL = "https://qr-test2.chinaums.com/netpay-route-server/api/";	
	
	//银联商务MD5密钥
	public static String MD5KEY = "fcAmtnx7MwismjWNhNKdHC44mNXtnEQeJkRrhKJwyrW2ysRR";
	
	//国光MD5密钥
	public static String GGMD5KEY = "IudaX2qWj33bjcL2IWgpUTa9LHFzDejU";
	
	public static final String FIELD_SIGN = "sign";
}
